package com.rustam.rustam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RustamApplication {

    public static void main(String[] args) {
        SpringApplication.run(RustamApplication.class, args);
    }

}
